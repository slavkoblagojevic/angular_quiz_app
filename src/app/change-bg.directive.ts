import { Directive, Input, HostListener, ElementRef } from '@angular/core';

@Directive({
  selector: '[appChangeBg]'
})
export class ChangeBgDirective {
  @Input() isCorrect: boolean | undefined;

  constructor(private elRef: ElementRef) { }

  @HostListener('click') changeStyle(){
    if(this.isCorrect){
      this.elRef.nativeElement.style.backgroundColor = 'green';
      this.elRef.nativeElement.style.color = 'white';
      this.elRef.nativeElement.style.border = '1px solid green';
    }else{
      this.elRef.nativeElement.style.backgroundColor = 'red';
      this.elRef.nativeElement.style.color = 'white';
      this.elRef.nativeElement.style.border = '1px solid red';
    }
  }

}
