export interface QuestionObject {
    questions: Question[]
  }
  
  export interface Question {
    questionText: string
    options: Option[]
    explanation: string
  }
  
  export interface Option {
    text: string
    correct?: boolean
  }
  