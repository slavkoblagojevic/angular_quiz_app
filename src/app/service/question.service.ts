import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { QuestionObject } from '../interface/question';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class QuestionService {

  constructor(private _http: HttpClient) { }

  getQuestionJson(): Observable<QuestionObject>{
    return this._http.get<QuestionObject>('assets/question2.json');
  }
}
