import { Component, ElementRef, ViewChild, OnInit, AfterViewInit } from '@angular/core';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.scss']
})
export class WelcomeComponent implements OnInit{
  @ViewChild('name') user_name!: ElementRef;
  
  ngOnInit(): void {
  }
  
  startQuiz(){
    console.log(this.user_name.nativeElement.value)
    localStorage.setItem('name', this.user_name.nativeElement.value);
  }
}
