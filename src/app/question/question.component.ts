import { Component, OnDestroy, OnInit } from '@angular/core';
import { QuestionService } from '../service/question.service';
import { Question, QuestionObject } from '../interface/question';
import { Subscription, interval } from 'rxjs';

@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.scss'],
})
export class QuestionComponent implements OnInit, OnDestroy {
  public userName: string | null = null;
  public questionList: Question[] = [];
  public currentQuestion: number = 0;
  public points: number = 0;
  /* public counter: any = 60; */
  public correctAnswer: number = 0;
  public inCorrectAnswer: number = 0;
  public interval$: Subscription = new Subscription();
  public progress: number = 0;
  public isQuizCompleted: boolean = false;
  public answered = false;
  constructor(private questionService: QuestionService) {}

  ngOnInit(): void {
    this.userName = localStorage.getItem('name');
    this.getAllQuestions();
 //   this.startCounter();
  }

  ngOnDestroy(): void {
  //  this.stopCounter();
  }

  getAllQuestions() {
    this.questionService.getQuestionJson().subscribe((response) => {
      console.log(response);
      this.questionList = response.questions;
      this.progress = ( 1 / (this.questionList.length)) * 100;
    });
  }

  previousQuestion() {
    this.currentQuestion--;
    console.log(this.currentQuestion);
  }

  nextQuestion() {
    this.currentQuestion++;
    this.getProgress();
    console.log(this.currentQuestion);
    this.answered = false;
  }

  answer(currentQuestion: any, option: any) {
    if(this.answered){
      return;
    }
    console.log(currentQuestion, option, this.questionList.length);

    if (option.correct) {
      this.points = this.points + 10;
      this.correctAnswer++;
      
      setTimeout(()=>{
        if(currentQuestion === this.questionList.length){
          console.log('kraj kviza')
          this.isQuizCompleted = true;
    //      this.stopCounter();
          return;
        }
/*         this.currentQuestion++;
        this.getProgress(); */
//this.resetCounter(); 
      },1000)
      this.answered = true;
    } else {
      this.inCorrectAnswer++;
      
      setTimeout(()=>{
        if(currentQuestion === this.questionList.length){
          console.log('kraj kviza')
          this.isQuizCompleted = true;
    //      this.stopCounter();
          return;
        }
        /* this.currentQuestion++;
        this.getProgress(); */
 //       this.resetCounter();
      },1000)
      this.answered = true;
    }
  }

  /* startCounter() {
    this.interval$ = interval(1000).subscribe((interval) => {
      console.log(interval); //every second will emit
      this.counter--;
      if (this.counter === 0) {
        this.interval$.unsubscribe();
        this.inCorrectAnswer++;
        this.counter = 60;
        this.currentQuestion++;
        this.startCounter();
      }
    });
  }

  stopCounter() {
    this.interval$.unsubscribe();
    this.counter = 0;
  }

  resetCounter() {
    this.stopCounter();
    this.counter = 60;
    this.startCounter();
  }
 */
  resetQuiz() {
    this.currentQuestion = 0;
   // this.resetCounter();
    this.getAllQuestions();
    this.points = 0;
    this.correctAnswer = 0;
    this.inCorrectAnswer = 0;
    this.progress = 0;
  }

  getProgress(): void {
    this.progress = ((this.currentQuestion + 1) / (this.questionList.length)) * 100;
  }
}
